#include <VirtualWire.h>
#include <OneWire.h> // Inclusion de la librairie OneWire
 
#define DS18B20 0x28     // Adresse 1-Wire du DS18B20
#define BROCHE_ONEWIRE 3 // Broche utilisée pour le bus 1-Wire
 
OneWire ds(BROCHE_ONEWIRE); // Création de l'objet OneWire ds
 
// Fonction récupérant la température depuis le DS18B20
// Retourne true si tout va bien, ou false en cas d'erreur
boolean getTemperature(float *temp){
  byte data[9], addr[8];
  // data : Données lues depuis le scratchpad
  // addr : adresse du module 1-Wire détecté
 
  if (!ds.search(addr)) { // Recherche un module 1-Wire
    ds.reset_search();    // Réinitialise la recherche de module
    return false;         // Retourne une erreur
  }
   
  if (OneWire::crc8(addr, 7) != addr[7]) // Vérifie que l'adresse a été correctement reçue
    return false;                        // Si le message est corrompu on retourne une erreur
 
  if (addr[0] != DS18B20) // Vérifie qu'il s'agit bien d'un DS18B20
    return false;         // Si ce n'est pas le cas on retourne une erreur
 
  ds.reset();             // On reset le bus 1-Wire
  ds.select(addr);        // On sélectionne le DS18B20
   
  ds.write(0x44, 1);      // On lance une prise de mesure de température
  delay(800);             // Et on attend la fin de la mesure
   
  ds.reset();             // On reset le bus 1-Wire
  ds.select(addr);        // On sélectionne le DS18B20
  ds.write(0xBE);         // On envoie une demande de lecture du scratchpad
 
  for (byte i = 0; i < 9; i++) // On lit le scratchpad
    data[i] = ds.read();       // Et on stock les octets reçus
   
  // Calcul de la température en degré Celsius
  *temp = ((data[1] << 8) | data[0]) * 0.0625; 
   


  // Pas d'erreur
  return true;
}
 
// setup()
void setup() {
    // RF: Initialise the IO and ISR
   vw_set_tx_pin(7);    // Pin de sortie du arduino (envoie des données au module RF)
   vw_setup(2000);      // Bits per sec

  
  Serial.begin(9600); // Initialisation du port série
  
}
 
// loop()
void loop() {
  float temp;
   
  // Lit la température ambiante à ~1Hz
  if(getTemperature(&temp)) {
     
    // Affiche la température
    Serial.print("Temperature : ");
    Serial.print(temp);
    Serial.write(176); // caractère °
    Serial.write('C');
    
    //On cast le float en char
    int temp1=(int)temp;
    int temp2=(int)((temp-temp1)*100);
    char msg[24];
    sprintf(msg,"%i.%i",temp1,temp2);
    
    //Envoie du signal en RF
    Serial.print(" / MSG envoye : ");
    Serial.print(msg);
      Serial.print("  // temp1= ");
      Serial.print(temp1);
      Serial.print("  // temp2= ");      Serial.print(temp2);
    Serial.println();
    digitalWrite(13, true);    // Flash a light to show transmitting
    vw_send((uint8_t *)msg, strlen(msg));
    vw_wait_tx();              // Wait until the whole message is gone
    digitalWrite(13, false);
    delay(200);
  }
}
